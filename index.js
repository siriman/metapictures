var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var twig = require('twig');
var sassMiddleware = require('node-sass-middleware');

var port = process.env.PORT || 9090;

var app = express();
var routes = require('./routes/routes');

// define which and where template view engine
app.set('view engine', 'twig');
app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(sassMiddleware({
    src: path.join(__dirname, 'public/css'),
    dest: path.join(__dirname, 'public/css'),
    sourceMap: true,
    outputStyle: 'compressed'
}));


app.use(routes);

app.use(express.static(path.join(__dirname, 'public')));


/*
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler (with stacktrace)
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
*/

app.listen(port, function () {
    console.log("App listen to port 9090");
});

module.exports = app;
