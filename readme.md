# MetaPictures
par Siriman TRAORE - Lotfi IDIR
# Description du projet
Ce projet consiste à réaliser une galerie d'images simple et permettre une consultation et édition des métadonnées. En plus cela, nous avons mis en place une possibilité de visualisation des photos sur une map, toute fois, si celles-ci contiennent des données géographiques et de partage sur les réseaux avec les micro-data, Open Graph, Twitter Card et Dublin Core.

Ce projet a été réalisé de telle sorte que les métadonnées ne soient pas extraites des images à chaque affichage. Cette optimisation n'inclut pas l'usage d'une base de données.

# Requirement
- NodeJs

# Installation

> `git clone git@gitlab.com:SirRoot/metapictures.git`
> `cd metapictures/`

> `npm install
>  npm start`

Vous pouvez commencer à tester l'application
Ouvrir le navigateur sur le [localhost:9090/](localhost:9090/)
