var dms = require('dms-conversion');
var fs = require('fs');
//var dataJson = require('../public/data/data');

var map = [];
var dataJson = function () {
    var d = fs.readFileSync('public/data/data.json');
    var data = JSON.parse(d);
    return data;
};

dataJson().forEach(function (m) {
    if((typeof m.GPSLatitude != 'undefined') && (typeof m.GPSLongitude != 'undefined')){
        var lat = m.GPSLatitude.replace("deg","°").replace(/ /g,'').replace("\"","\" ");
        var lng = m.GPSLongitude.replace("deg","°").replace(/ /g,'').replace("\"","\" ");
        var latLng = [lat, lng];
        var coord = {
            id: m.CurrentIPTCDigest,
            image: m.FileName,
            lat: latLng.map(dms.parseDms)[0],
            lng: latLng.map(dms.parseDms)[1]
        };
        map.push(coord);
    }
});

fs.writeFileSync('public/data/map.json', JSON.stringify(map));

var indexController = {
    index: function (req, res) {
        res.render('index', {
            title: "MetaPictures",
            metaDescription: "Projet about metadata for images",
            metaAuteur: "Lotfi IDIR, Siriman TRAORÉ",
            metaUrl : "https://metapictures.herokuapp.com/",
            metaImage: "",
            data: dataJson()

        });
    },
    map: function (req, res) {
        res.render('map', {
            title: "MetaPictures",
            metaDescription: "Projet about metadata for images",
            metaAuteur: "Lotfi IDIR, Siriman TRAORÉ",
            metaUrl : "https://metapictures.herokuapp.com/map",
            metaImage: "",
            coord: JSON.stringify(map)
        });
    }
};

module.exports = indexController;