var multer = require('multer');
var exec = require('child_process').execFile;
var exiftool = require('dist-exiftool');
var path = require('path');
var fs = require('fs');
var Promise = require('promise');

var MAGIC_NUMBERS = {
    jpg: 'ffd8ffe0',
    jpg1: 'ffd8ffe1',
    png: '89504e47',
    gif: '47494638'
};

var checkMagicNumbers = function (magic) {
    if (magic == MAGIC_NUMBERS.jpg
        || magic == MAGIC_NUMBERS.jpg1
        || magic == MAGIC_NUMBERS.png
        || magic == MAGIC_NUMBERS.gif)
        return true
};

var meta = function (file) {
    return new Promise(function (resolve, reject) {
        fs.readFile(file, function (err, data) {
            if (err) console.log(err);
            else {
                exec(exiftool, ['-j', file], function (error, stdout, stderr) {
                    if (error) {
                        reject(error);
                        console.error("exec error" + error);
                        return;
                    }
                    resolve(writeData(stdout));
                });
                exec(exiftool, ['-xmp', '-b', file], function (error, stdout, stderr) {
                    if (error) {
                        console.error("exec error" + error);
                        return;
                    }
                    var filename = file.substring(0, 34);
                    fs.writeFile(filename + '.xmp', stdout);
                });
            }
        });
        console.log('Metadata registered');
    });
};

var writeData = function (metadata) {
    metadata = JSON.parse(metadata)[0];
    fs.readFile("public/data/data.json", function (err, data) {
        if (data === []) {
            data.push(metadata);
            fs.writeFile("public/data/data.json", data);
        } else {
            data = JSON.parse(data);
            for (var index = 0; index < data.length; index++) {
                var element = data[index];
                if(element.FileName == metadata.FileName){
                    data.splice(index, 1); 
                }
            }
            data.push(metadata);
            fs.writeFile("public/data/data.json", JSON.stringify(data));
        }
    });
};

var uploadImage = function (req, res) {
    var upload = multer({
        storage: multer.memoryStorage()
    }).single('image');
    return new Promise(function (resolve, reject) {
        upload(req, res, function (err) {
            var buffer = req.file.buffer;
            var magic = buffer.toString('hex', 0, 4);
            var filename = req.file.fieldname + '-' + Date.now() + path.extname(req.file.originalname);
            if (checkMagicNumbers(magic)) {
                fs.writeFile('public/uploads/' + filename, buffer, 'binary', function (err) {
                    if (err) console.log(err);
                    console.log('File is upload');
                    resolve(filename);
                });
            } else {
                reject(
                    res.end('File is no valid')
                );
            }
        });
    });
};

function getImage(identifiant){
    var d = fs.readFileSync('public/data/data.json');
    var data = JSON.parse(d);
    var newData = data.filter(function (d) {
        return d.CurrentIPTCDigest === identifiant;
    });
    return newData;
}

var getAllData = function () {
    var d = fs.readFileSync('public/data/data.json');
    var data = JSON.parse(d);
    return data;
};

var imageController = {
    add: function (req, res) {
        if (req.method === "POST") {
            uploadImage(req, res)
                .then(function (filename) {
                    meta('public/uploads/' + filename)
                    .then(function(){
                        res.redirect('/');                        
                    });
                });
            return;
        }
        res.render('images/add');
    },
    details: function (req, res) {
        newData = getImage(req.params.id);
        delete newData[0].SourceFile;
        delete newData[0].ExifToolVersion;
        delete newData[0].Directory;
        delete newData[0].FileModifyDate;
        delete newData[0].FileAccessDate ;
        delete newData[0].FileCreateDate ;
        delete newData[0].FilePermissions ;
        res.render('images/details', {
            title: newData[0].Title,
            description: newData[0].Description,
            image: newData[0],
            metaDescription: newData[0].Description,
            metaAuteur: newData[0].Artist,
            metaUrl : "https://metapictures.herokuapp.com/image/" + req.params.id,
            metaImage: newData[0].Source,
            linkXmp: newData[0].FileName.substr(0, newData[0].FileName.lastIndexOf(".")) + ".xmp"
        });
    },
    edit: function (req, res) {
        if (req.method === "GET") { 
            imageData = getImage(req.params.id);
            var unEditable = ['SourceFile', 'Directory', 'FileName', 'CurrentIPTCDigest' ];
            res.render('images/edit', {
                image:imageData[0],
                tabUneditable:unEditable
            });
        }

        if (req.method === "POST") { 
            var originalData = getImage(req.params.id)[0];
            var mdata = req.body;
            var arg = [];
            var path = originalData['SourceFile'];
            for(key in mdata){
                for(k in originalData){                    
                    if(k === key){
                        if(mdata[key] != originalData[k]){
                            arg.push("-" + key + "=" + mdata[key]);
                        }
                    }
                }
            }
            arg.push(path)
            exec(exiftool, arg, function (error, stdout, stderr) {
                if (error) {
                    console.error("exec error" + error);
                    return;
                }
                meta(path).then(function(){
                    res.redirect('/image/'+req.params.id);                    
                });
            });
        }
    },
    delete: function (req, res) {
        if (req.method === "GET") {
            res.render('images/delete');
        }
        if (req.method === "POST") {
        }
    },
    about: function (req, res) {
        if (req.method === "GET") {
            res.render('about');
        }
    }
};

module.exports = imageController;