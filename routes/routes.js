var express = require('express');
var router = express.Router();

var indexController = require('../controllers/indexController');
var imageController = require('../controllers/imageController');

router.get('/', indexController.index);
router.get('/map', indexController.map);

router.get('/image/add', imageController.add);
router.post('/image/add', imageController.add);
router.get('/image/:id', imageController.details);
router.get('/image/edit/:id', imageController.edit);
router.post('/image/edit/:id', imageController.edit);
router.get('/image/delete/:id', imageController.delete);
router.post('/image/delete/:id', imageController.delete);
router.get('/about', imageController.about);

module.exports = router;