window.onload = function () {
        var xhr = new XMLHttpRequest();
        var url = "data/map.json";
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                var coord = JSON.parse(xhr.responseText);
                initMap(coord);
            }
        };
        xhr.open("GET", url, true);
        xhr.setRequestHeader('Accept', 'application/json');
        xhr.send();

    function initMap(coord) {
        var bounds = new google.maps.LatLngBounds();
        var latLng = {lat: -25.363, lng: 131.044};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: latLng
        });
        coord.forEach(function (value) {
            var position = new google.maps.LatLng(value.lat, value.lng);
            var icon = {
                url: "../uploads/"+ value.image,
                scaledSize: new google.maps.Size(50, 50),
                origin: new google.maps.Point(0,0),
                anchor: new google.maps.Point(0, 0)
            };

            var marker = new google.maps.Marker({
                position : position,
                map: map,
                icon: icon
            });
            marker.addListener('click', function() {
                location = window.location;
                location.href = "http://localhost:9090/image/"+value.id;
            });      
            bounds.extend(position);
        });
        map.fitBounds(bounds);
    }
};
